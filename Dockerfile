FROM ruby:2.1
WORKDIR /usr/src/app
COPY ./application/Gemfile /usr/src/app
COPY ./application/Gemfile.lock /usr/src/app
RUN bundle install
COPY ./application/ /usr/src/app
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
ENV RACK_ENV production
CMD ["bundle", "exec", "ruby", "./app.rb"]
