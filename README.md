# About #

URL shortener app based on Sinatra framework.

## Requirements ##
* Ruby 2 and above
* Gems
    * sinatra - framework
    * puma - application server
    * redis - integration with redis  
* Redis (K/V storage) is expected on localhost

## Run ##

### App as Docker container ###
* `docker build -t url_shortener .`
* `docker run -p 127.0.0.1:80:4567 --name app -d url_shortener`
* Application is available on http://127.0.0.1/

### Run set of containers ###
`docker-compose up --build`

### Development environment ###

#### Prerequisites ####
* Install ruby 2.*
* Install redis or run it as Docker container: 
`docker run -p 127.0.0.1:6379:6379 --name redis -d redis`
* `gem install bundler`
* `bundle install`

#### Run ####

* fish: `env RACK_ENV=development bundle exec ruby ./app.rb`
* bash: `RACK_ENV=development bundle exec ruby ./app.rb`

Application is available on http://localhost:4567/