class Storage
  def connect(role)
    sentinel_ip = `ip r | awk '/default/{print $3}'`.strip
    Redis.new(:url => "redis://mymaster", :sentinels => [{:host => sentinel_ip, :port => 26379}], :role => role)
    # Redis.new(:host => 'redis')
  end

  def getMaster()
    self.connect(:master)
  end

  def getSlave()
    self.connect(:slave)
  end

  def get(key)
    redis = self.getSlave()
    redis.get(key)
  end

  def set(key, value)
    redis = self.getMaster()
    redis.set(key, value)
  end

  def exists(key)
    redis = self.getSlave()
    redis.exists(key)
  end

  def del(key)
    redis = self.getMaster()
    redis.del(key)
  end

  def ping()
    redis = self.getSlave()
    redis.ping
  end
end