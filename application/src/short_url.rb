class ShortUrl < Storage
  attr_accessor :key
  attr_accessor :target

  def generate(url)
    @target = url
    @key = self.setKey()
    @key if self.set(self.key, self.target) == "OK"
  end

  def getTarget(key)
    return self.get(key)
  end

  def setKey()
    key = SecureRandom.urlsafe_base64(4)
    return key unless self.exists(key)
    self.setKey()
  end

end