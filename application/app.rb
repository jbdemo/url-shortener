#!/usr/bin/env ruby

require "sinatra"
require "sinatra/cookies"
require "newrelic_rpm"
require "redis"
require "securerandom"
require 'uri'
require "./src/storage"
require "./src/short_url"

configure do
  set :server, :puma
  set :redis_host, "redis"
end

before do
  begin
    @url = ShortUrl.new()
    @url.ping
  rescue Exception => e
    cookies[:error] = 'Redis is not available. Error: ' + e.message
    redirect '/' unless request.path == '/'
  end
end

after '/' do
  cookies.delete(:error)
end

get '/' do
  erb :index
end

get /go\/([A-Za-z0-9\-_]{6})\// do |key|
  redirect @url.getTarget(key)
end

get /view\/([A-Za-z0-9\-_]{6})\// do |key|
  long = Rack::Utils.escape_html(@url.getTarget(key))
  short = request.base_url + '/go/' + key + '/'
  erb :url, :locals => {:short => short, :long => long}
end

post '/new/' do
  target = params['url']
  if target =~ URI::regexp && target.instance_of?(String) && target.length < 2000
    key = @url.generate(params['url'])
    redirect '/view/' + key + '/'
  else
    cookies[:error] = 'Looks like you filled invalid URL.
                       Please check if it has prefix "http://" or "https://"'
    redirect '/'
  end
end
